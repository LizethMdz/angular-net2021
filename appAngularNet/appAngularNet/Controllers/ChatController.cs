﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using appAngularNet.Models;
using appAngularNet.Models.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace appAngularNet.Controllers
{
    [Route("api/[controller]")]
    public class ChatController : Controller
    {
        private Models.MyDBContext db;
        public ChatController(Models.MyDBContext context)
        {
            db = context;
        }
        [HttpGet("[action]")]
        public IEnumerable<MessageViewModel> Message()
        {
            List<MessageViewModel> lst = (from d in db.Message
                                         select new MessageViewModel
                                         {
                                             Id = d.Id,
                                             Name = d.Name,
                                             Text = d.Text
                                         }).ToList();
            return lst;
        }

        [HttpPost("[action]")]
        public ResponseMessage AgregarMensaje ([FromBody] MessageViewModel mensaje)
        {
            try
            {
                Models.Messages oMessage = new Models.Messages();
                oMessage.Name = mensaje.Name;
                oMessage.Text = mensaje.Text;
                db.Message.Add(oMessage);
                db.SaveChanges();

                ResponseMessage responseMessage = new ResponseMessage()
                {
                    Success = true,
                    Message = "Mensaje creado",
                    Data = null
                };
                return responseMessage;
            }
            catch(Exception ex)
            {
                ResponseMessage responseMessage = new ResponseMessage()
                {
                    Success = false,
                    Message = ex.Message,
                    Data = null
                };
                return responseMessage;
            }
        }
    }
}