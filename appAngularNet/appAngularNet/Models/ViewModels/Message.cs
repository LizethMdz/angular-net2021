﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appAngularNet.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
