﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appAngularNet.Models.ResponseModels
{
    public class ResponseMessage
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
