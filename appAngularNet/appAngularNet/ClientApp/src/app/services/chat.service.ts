import { Message } from './../interfaces/chat.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const url = environment.urlBack;
const httpHeaders = new HttpHeaders({
  'Content-Type': 'application/json',
  'Authorization': 'token'
})
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  getMensajes() {
    return this.http.get(`${url}api/Chat/Message`);
  }

  postMensajes(request: Message) {
    return this.http.post(`${url}api/Chat/AgregarMensaje`, request, {
      headers: httpHeaders
    });
  }

}
