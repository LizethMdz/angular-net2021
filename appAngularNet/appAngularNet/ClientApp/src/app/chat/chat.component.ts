import { ChatService } from '../services/chat.service';
import { Component, OnInit } from '@angular/core';
import { Message } from '../interfaces/chat.interface';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  list: Message[] = [];
  formChat: FormGroup = new FormGroup({
    Name: new FormControl(''),
    Text: new FormControl(''),
  })
  constructor(private chatS: ChatService) { }

  ngOnInit() {
    this.getMensajes();
  }

  addMensaje() {
    this.chatS.postMensajes(this.formChat.value).subscribe((data: Message[]) => {
      console.log(data);
      this.getMensajes();
    });
  }

  getMensajes() {
    this.chatS.getMensajes().subscribe((data: Message[]) => {
      console.log(data)
      this.list = data;
    });
  }

}
