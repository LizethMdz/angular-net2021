import { ResponseWS } from './../interfaces/clientes.model';
import { Observable } from 'rxjs';
import { Venta } from './../interfaces/ventas.models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

const url = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  constructor(private http: HttpClient) { }
  
  postAgregarVenta(venta: Venta) : Observable<ResponseWS> {
    return this.http.post<ResponseWS>(url + "venta", venta)
  }

}
