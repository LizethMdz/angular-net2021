import { RequestCliente } from './../interfaces/clientes.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResponseWS } from '../interfaces/clientes.model';

const url = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }

  getClientes(): Observable<ResponseWS> {
    return this.http.get<ResponseWS>(url + "cliente");
  }
  postCliente(request: RequestCliente): Observable<ResponseWS> {
    return this.http.post<ResponseWS>(url + "cliente", request);
  }
  putCliente(request: RequestCliente): Observable<ResponseWS> {
    return this.http.put<ResponseWS>(url + "cliente", request);
  }
  deleteCliente(id: number): Observable<ResponseWS> {
    return this.http.delete<ResponseWS>(url + "cliente/" + id);
  }
}
