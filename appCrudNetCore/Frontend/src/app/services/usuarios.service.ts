import { Login } from './../interfaces/login.model';
import { ResponseWS } from './../interfaces/clientes.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { usuario } from '../interfaces/usuarios.models';
import { map } from "rxjs/operators";

const url = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private usuarioSubject = new BehaviorSubject<any>(null);
  currentValueUsuario = this.usuarioSubject.asObservable();

  public cambiarUsuarioValue (usuario: usuario) {
    this.usuarioSubject.next(usuario);
  }

  constructor(private http: HttpClient) { }

  login(request: Login): Observable<ResponseWS> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ResponseWS>(url + "usuario/login", request, {headers}).pipe(
      map(resp => {
        if(resp.exito) {
          const user: usuario = resp.data;
          this.cambiarUsuarioValue(user);
        }
        return resp;
      })
    );
  }

  logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    this.usuarioSubject.next(null);
  }

}
