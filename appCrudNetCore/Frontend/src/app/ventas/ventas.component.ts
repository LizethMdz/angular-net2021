import { DialogVentaComponent } from './@components/dialog-venta/dialog-venta.component';
import { DialogAgregarEditarComponent } from './../cliente/@components/dialog-agregar/dialog-agregar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private snackbar : MatSnackBar
  ) { }

  ngOnInit(): void {
  }

  agregarVentaDialog(){
    const dialogRef = this.dialog.open(DialogVentaComponent,
      {
        width : '600px'
      });
  }

}
