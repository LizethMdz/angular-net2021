import { Concepto, Venta } from './../../../interfaces/ventas.models';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { VentaService } from './../../../services/venta.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog-venta',
  templateUrl: './dialog-venta.component.html',
  styleUrls: ['./dialog-venta.component.css']
})
export class DialogVentaComponent implements OnInit {

  conceptos! : Concepto[];
  venta! : Venta;

  formVenta : FormGroup = new FormGroup({
    Cantidad : new FormControl(0, Validators.required),
    IdProducto : new FormControl(0, Validators.required),
    Importe : new FormControl(0, Validators.required)
  });

  constructor(
    private dialogRef : MatDialogRef<DialogVentaComponent>,
    private snackBar : MatSnackBar,
    private ventaS : VentaService
  ) { }

  ngOnInit(): void {
    this.conceptos = [];
    this.venta = {
      IdCliente : 2,
      Conceptos : this.conceptos,
    }
  }

  close(){
    this.dialogRef.close();
  }

  agregarConceptos(){
    this.conceptos.push(this.formVenta.getRawValue());
  }

  agregarVenta() {
    this.venta.Conceptos = this.conceptos;
    console.log(this.venta);
    // this.ventaS.postAgregarVenta(this.venta).subscribe(response => {
    //   if (response.exito) {
    //     this.dialogRef.close();
    //     this.snackBar.open(response.mensaje, "Cerrar", {
    //       duration: 3000
    //     });
    //   }
    // });
  }



}
