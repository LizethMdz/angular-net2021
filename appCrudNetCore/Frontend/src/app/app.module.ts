import { JwtInterceptor } from './security/jwt.interceptor';
import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { ClienteComponent } from './cliente/cliente.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DialogAgregarEditarComponent } from './cliente/@components/dialog-agregar/dialog-agregar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogBorrarComponent } from './cliente/@components/dialog-borrar/dialog-borrar.component';
import { LoginComponent } from './login/login.component';
import { VentasComponent } from './ventas/ventas.component';
import { DialogVentaComponent } from './ventas/@components/dialog-venta/dialog-venta.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClienteComponent,
    DialogAgregarEditarComponent,
    DialogBorrarComponent,
    LoginComponent,
    VentasComponent,
    DialogVentaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide : HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
