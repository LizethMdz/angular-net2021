import { Router } from '@angular/router';
import { UsuariosService } from './../services/usuarios.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credenciales : FormGroup = new FormGroup({
    email : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required)
  });
  
  constructor(private usuarioS: UsuariosService, private route: Router) { }

  ngOnInit() {
    if (localStorage.getItem('token')) { this.route.navigate(['clientes']); }

    this.usuarioS.currentValueUsuario.subscribe(usuario => {
      if(usuario) {
        console.log(usuario);
      }
    })
  }

  login() {
    const request = { ...this.credenciales.getRawValue() };
    this.usuarioS.login(request).subscribe(response => {
      console.log(response);
      if(response.exito)
        this.route.navigate(['clientes']);
    });
  }

}
