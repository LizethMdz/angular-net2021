export interface ResponseWS {
    exito: number;
    mensaje: string;
    data: any;
}

export interface RequestCliente {
    Id?: number;
    Nombre: string;
}