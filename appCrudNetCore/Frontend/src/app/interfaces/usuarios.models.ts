export interface usuario {
    email: string;
    jwtToken : string;
    refreshToken? : any;
}