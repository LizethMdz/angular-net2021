export interface Venta {
    IdCliente : number;
    Conceptos : Concepto[];
}

export interface Concepto {
    Cantidad : number;
    IdProducto : number;
    Importe : number;
}