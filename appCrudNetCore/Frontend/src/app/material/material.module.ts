import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogConfig } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatTabsModule } from '@angular/material/tabs';

const MAT_MODULES = [
  MatSidenavModule,
  MatToolbarModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatListModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatCardModule,
  MatButtonToggleModule,
  MatSliderModule,
  MatDialogModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatTableModule,
  MatChipsModule,
  MatTabsModule,
];

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [...MAT_MODULES,],
  declarations: []
})
export class MaterialModule { }
