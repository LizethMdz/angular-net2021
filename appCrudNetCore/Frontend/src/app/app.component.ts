import { Router } from '@angular/router';
import { UsuariosService } from './services/usuarios.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { usuario } from './interfaces/usuarios.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'appWSVentasFront';
  mobileQuery: MediaQueryList;
  usuario! : usuario;

  menu = [
    { name: 'Home', route: 'home', icon: 'home' },
    { name: 'Clientes', route: 'clientes', icon: 'person' },
    { name: 'Ventas', route: 'ventas', icon: 'inventory_2' },
    // { name: 'Logout', route: 'login', icon: 'clear' },
  ];

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router : Router,
    private usuarioS: UsuariosService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.usuarioS.currentValueUsuario.subscribe(usuario => {
      if(usuario) {
        this.usuario = usuario;
        console.log("Hubo un cambio en el usuario",usuario);
      }
    })
  }

  shouldRun = true;

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logout(){
    this.usuarioS.logout();
    this.router.navigate(['login']);
  }
}

