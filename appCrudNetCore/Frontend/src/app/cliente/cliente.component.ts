import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogBorrarComponent } from './@components/dialog-borrar/dialog-borrar.component';
import { RequestCliente } from './../interfaces/clientes.model';
import { DialogAgregarEditarComponent } from './@components/dialog-agregar/dialog-agregar.component';
import { ClienteService } from './../services/cliente.service';
import { Component, OnInit } from '@angular/core';
import { ResponseWS } from '../interfaces/clientes.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  clientes: any[] = [];
  displayedColumns = [
    "id_cliente",
    "nombre",
    "actions"
  ];
  dataSource!: MatTableDataSource<any>;
  constructor(private clientesS: ClienteService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.obtenerClientes();
  }

  obtenerClientes() {
    this.clientesS.getClientes().subscribe(clientes => {
      this.clientes = clientes.data;
      this.dataSource = new MatTableDataSource(this.clientes);
    });
  }

  agregar() {
    const dialogRef = this.dialog.open(DialogAgregarEditarComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.obtenerClientes();
    });
  }

  editar(cliente: RequestCliente) {
    const dialogRef = this.dialog.open(DialogAgregarEditarComponent, {
      width: '250px',
      data: cliente
    });

    dialogRef.afterClosed().subscribe(result => {
      this.obtenerClientes();
    });
  }

  borrar(idCliente: number) {
    const dialogRef = this.dialog.open(DialogBorrarComponent, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) this.clientesS.deleteCliente(idCliente).subscribe(response => {
        if (response.exito) {
          this.obtenerClientes();
          this.snackBar.open(response.mensaje, "Cerrar", {
            duration: 3000
          });
        }
      });
    });
  }

}
