import { RequestCliente } from './../../../interfaces/clientes.model';
import { ClienteService } from './../../../services/cliente.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dialog-agregar',
  templateUrl: './dialog-agregar.component.html',
  styleUrls: ['./dialog-agregar.component.css']
})
export class DialogAgregarEditarComponent implements OnInit {

  nombreCliente: string = "";
  constructor(public dialogRef: MatDialogRef<DialogAgregarEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private clienteS: ClienteService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    if (this.data) this.nombreCliente = this.data.nombre;
    console.log(this.nombreCliente)
  }

  close() {
    this.dialogRef.close();
  }

  addCliente() {
    const request: RequestCliente = { Nombre: this.nombreCliente }
    this.clienteS.postCliente(request).subscribe(response => {
      if (response.exito) {
        this.dialogRef.close();
        this.snackBar.open(response.mensaje, "Cerrar", {
          duration: 3000
        });
      }
    });
  }


  editCliente() {
    const request: RequestCliente = { ...this.data, Nombre: this.nombreCliente }
    this.clienteS.putCliente(request).subscribe(response => {
      if (response.exito) {
        this.dialogRef.close();
        this.snackBar.open(response.mensaje, "Cerrar", {
          duration: 3000
        });
      }
    });
  }

}
