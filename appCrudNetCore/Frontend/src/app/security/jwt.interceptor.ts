import { usuario } from './../interfaces/usuarios.models';
import { Observable, throwError } from 'rxjs';
import { UsuariosService } from './../services/usuarios.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
    usuarioActual = null;
    constructor(private usuarioS: UsuariosService) {
        this.usuarioS.currentValueUsuario.subscribe(usuario => {
            this.usuarioActual = usuario;
        })
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = request.url;
        const dataUsuario = this.usuarioActual;
        if (!request.headers.has('Content-Type')) {
            request = request.clone({
                headers: request.headers.set('Content-Type', 'application/json'),
            });
        }
        request = request.clone({
            headers: request.headers.set('Accept', 'application/json'),
        });
        //Procesar request
        return next.handle(this.agregarToken(request)).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (url.includes('/login')) {
                        const dato = event.body.data;
                        console.log(dato.jwtToken);
                        localStorage.setItem('token', dato.jwtToken);
                        localStorage.setItem('refreshToken', dato['refreshToken']['token']);
                    }
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                console.log("error")
                if (!url.includes('/login')) {
                    if (error.status === 401 || error.status === 0) {
                        this.usuarioS.logout();
                    }
                }
                return throwError(error);
            })
        );
    }

    agregarToken(request: HttpRequest<any>) {
        const token = localStorage.getItem('token');
        if (token && !request.url.includes('/login') && !request.url.includes('token/refresh')) {
            return request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }
        return request;
    }
}