import { usuario } from './../interfaces/usuarios.models';
import { UsuariosService } from './../services/usuarios.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  usuarioActual = null;
  constructor(private router: Router, private usuarioS: UsuariosService) { 
    this.usuarioS.currentValueUsuario.subscribe(usuario => {
      this.usuarioActual = usuario;
    })
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const usuario = localStorage.getItem("token");
      const dataUsuario = this.usuarioActual;
      if(dataUsuario || usuario) return true;
      this.router.navigate(['/login'])
      return false;
  }

}
