﻿using System;
using System.Collections.Generic;

#nullable disable

namespace appWSVentas.Models
{
    public partial class Concepto
    {
        public int Id { get; set; }
        public int IdProducto { get; set; }
        public int IdVenta { get; set; }
        public decimal Importe { get; set; }
        public decimal PrecioUnitario { get; set; }
        public int Cantidad { get; set; }

        public virtual Producto Producto { get; set; }
        public virtual Ventum Ventum { get; set; }
    }
}
