﻿using appWSVentas.Entities;
using appWSVentas.Models.RequestWS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace appWSVentas.Models.ResponseWS
{
    public class ResponseUsuarioModel
    {
        public string Email { get; set; }
        public string JwtToken { get; set; }
        public RefreshToken RefreshToken { get; set; }

        //public ResponseUsuarioModel(RequestUsuarioModel user, string jwtToken, string refreshToken)
        //{
        //    Email = user.Email;
        //    JwtToken = jwtToken;
        //    RefreshToken = refreshToken;
        //}
    }
}
