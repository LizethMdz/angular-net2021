﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace appWSVentas.Models.RequestWS
{
    public class RequestVentaModel
    {
        //public int Id { get; set; }
        [Required]
        [ExisteCliente(ErrorMessage = "El cliente no exite")]
        [Range(1, Double.MaxValue, ErrorMessage = "El campo IdCliente es requerido")]
        public int IdCliente { get; set; }
        //public decimal Total {get; set;}
        [Required]
        [MinLength(1, ErrorMessage = "Deben existir conceptos")]
        public List<Concepto> Conceptos { get; set; }

        public RequestVentaModel()
        {
            this.Conceptos = new List<Concepto>();
        }
    }

    public class Concepto
    {
        //public int Id { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Importe { get; set; }
        public int IdProducto { get; set; }
    }

    #region Validaciones
    public class ExisteCliente : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int idCliente = (int)value;
            using (VentaRealDbContext db = new VentaRealDbContext())
            {
                if (db.Clientes.Find(idCliente) == null) return false;
            }
            return true;
        }
    }
    #endregion
}
