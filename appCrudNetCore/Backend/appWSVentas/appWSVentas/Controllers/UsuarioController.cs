﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using appWSVentas.Interfaces;
using appWSVentas.Models.RequestWS;
using appWSVentas.Models.ResponseWS;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace appWSVentas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private IUsuarioService _usuarioService;
        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }
        [HttpPost("login")]
        public IActionResult Login([FromBody] RequestUsuarioModel usuario)
        {
            ResponseClienteModel respuesta = new ResponseClienteModel();
            var usuarioResponse = _usuarioService.Login(usuario, ipAddress());
            if (usuarioResponse == null)
            {
                respuesta.Exito = 0;
                respuesta.Data = null;
                respuesta.Mensaje = "Usuario o contraseña incorrectos";

                return BadRequest(respuesta);
            }

            respuesta.Exito = 1;
            respuesta.Data = usuarioResponse;
            return Ok(respuesta);
        }

        private string ipAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
