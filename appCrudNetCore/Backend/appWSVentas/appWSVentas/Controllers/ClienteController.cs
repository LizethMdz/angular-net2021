﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using appWSVentas.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using appWSVentas.Models;
using appWSVentas.Models.ResponseWS;
using appWSVentas.Models.RequestWS;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace appWSVentas.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    //[EnableCors("*", "*", "*")]
    public class ClienteController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            ResponseClienteModel respuestaClienteModel = new ResponseClienteModel();
            try
            {
                using (VentaRealDbContext db = new VentaRealDbContext())
                {
                    var lst = db.Clientes.OrderByDescending(d => d.Id).ToList();
                    respuestaClienteModel.Exito = 1;
                    respuestaClienteModel.Mensaje = "Respuesta exitosa";
                    respuestaClienteModel.Data = lst;
               
                }
            }
            catch(Exception ex)
            {
                respuestaClienteModel.Exito = 0;
                respuestaClienteModel.Mensaje = $"No se pudo obtener la información, {ex.Message}";
            }
            return Ok(respuestaClienteModel);
        }

        [HttpPost]
        public IActionResult Add([FromBody] RequestClienteModel clienteModel)
        {
            ResponseClienteModel respuestaClienteModel = new ResponseClienteModel();
            try
            {
                using (VentaRealDbContext db = new VentaRealDbContext())
                {
                    Cliente oCliente = new Cliente();
                    oCliente.Nombre = clienteModel.Nombre;
                    db.Clientes.Add(oCliente);
                    db.SaveChanges();
                    respuestaClienteModel.Exito = 1;
                    respuestaClienteModel.Mensaje = "Se ha guardado el cliente";
                    

                }
            }
            catch (Exception ex)
            {
                respuestaClienteModel.Exito = 0;
                respuestaClienteModel.Mensaje = $"No se pudo guardar la información, {ex.Message}";
            }
            return Ok(respuestaClienteModel);
        }

        [HttpPut]
        public IActionResult Edit(RequestClienteModel clienteModel)
        {
            ResponseClienteModel respuestaClienteModel = new ResponseClienteModel();
            try
            {
                using (VentaRealDbContext db = new VentaRealDbContext())
                {
                    Cliente oCliente = db.Clientes.Find(clienteModel.Id);
                    oCliente.Nombre = clienteModel.Nombre;
                    db.Entry(oCliente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                    respuestaClienteModel.Exito = 1;
                    respuestaClienteModel.Mensaje = "Se ha actualizado el cliente";


                }
            }
            catch (Exception ex)
            {
                respuestaClienteModel.Exito = 0;
                respuestaClienteModel.Mensaje = $"No se pudo actualizar la información, {ex.Message}";
            }
            return Ok(respuestaClienteModel);
        }

        [HttpDelete("{Id}")]
        public IActionResult Delete(int Id)
        {
            ResponseClienteModel respuestaClienteModel = new ResponseClienteModel();
            try
            {
                using (VentaRealDbContext db = new VentaRealDbContext())
                {
                    Cliente oCliente = db.Clientes.Find(Id);

                    db.Remove(oCliente);
                    db.SaveChanges();
                    respuestaClienteModel.Exito = 1;
                    respuestaClienteModel.Mensaje = "Cliente borrado con exito";


                }
            }
            catch (Exception ex)
            {
                respuestaClienteModel.Exito = 0;
                respuestaClienteModel.Mensaje = $"No se pudo borrar la información, {ex.Message}";
            }
            return Ok(respuestaClienteModel);
        }

    }

    
}