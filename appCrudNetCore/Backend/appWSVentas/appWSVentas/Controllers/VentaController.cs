﻿using appWSVentas.Models.RequestWS;
using appWSVentas.Models.ResponseWS;
using appWSVentas.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using appWSVentas.Services;
using appWSVentas.Interfaces;

namespace appWSVentas.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VentaController : ControllerBase
    {

        private IVentaService _ventaService;
        public VentaController(IVentaService ventaService)
        {
            _ventaService = ventaService;
        }
        [HttpPost]
        public IActionResult Add([FromBody] RequestVentaModel venta)
        {
            ResponseClienteModel respuestaClienteModel = new ResponseClienteModel();
            try
            {
                _ventaService.AgregarVenta2(venta);
                respuestaClienteModel.Exito = 1;
                respuestaClienteModel.Mensaje = "Se ha guardado la venta";
            }
            catch (Exception ex)
            {
                respuestaClienteModel.Exito = 0;
                respuestaClienteModel.Mensaje = $"No se pudo guardar la información, {ex.Message}";
            }
            return Ok(respuestaClienteModel);
        }
    }
}
