﻿using appWSVentas.Interfaces;
using appWSVentas.Models;
using appWSVentas.Models.RequestWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appWSVentas.Services
{
    public class VentaService : IVentaService
    {
        public void AgregarVenta(RequestVentaModel venta)
        {
            using (VentaRealDbContext db = new VentaRealDbContext())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Ventum oVenta = new Ventum();
                        oVenta.Total = venta.Conceptos.Sum(d => d.Cantidad * d.PrecioUnitario);
                        oVenta.Fecha = DateTime.Now;
                        oVenta.IdCliente = venta.IdCliente;
                        db.Venta.Add(oVenta);
                        db.SaveChanges();

                        foreach (var item in venta.Conceptos)
                        {
                            var concepto = new Models.Concepto();
                            concepto.IdProducto = item.IdProducto;
                            concepto.Cantidad = item.Cantidad;
                            concepto.PrecioUnitario = item.PrecioUnitario;
                            //concepto.IdVenta = venta.Id;
                            concepto.Importe = item.Importe;
                            db.Conceptos.Add(concepto);
                            db.SaveChanges();
                        }
                        transaction.Commit();
                            
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("No se agregó la venta");
                    }

                }
            }
        }

        public void AgregarVenta2(RequestVentaModel venta)
        {
            using (VentaRealDbContext db = new VentaRealDbContext())
            {

                //    using (var transaction = db.Database.BeginTransaction())
                //    {
                try
                    {
                        Ventum oVenta = new Ventum();
                        oVenta.Total = venta.Conceptos.Sum(d => d.Cantidad * d.PrecioUnitario);
                        oVenta.Fecha = DateTime.Now;
                        oVenta.IdCliente = venta.IdCliente;
                        db.Venta.Add(oVenta);
                        db.SaveChanges();

                        foreach (var item in venta.Conceptos)
                        {
                            var concepto = new Models.Concepto();
                            concepto.IdProducto = item.IdProducto;
                            concepto.Cantidad = item.Cantidad;
                            concepto.PrecioUnitario = item.PrecioUnitario;
                            //concepto.IdVenta = venta.Id;
                            concepto.Importe = item.Importe;
                            db.Conceptos.Add(concepto);
                            db.SaveChanges();
                        }
                        //transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        //transaction.Rollback();
                        throw new Exception($" {ex.Message}");
                    }

                //    }
            }
        }
    }
}
