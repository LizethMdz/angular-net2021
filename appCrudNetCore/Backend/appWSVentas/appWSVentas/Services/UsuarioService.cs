﻿using appWSVentas.Entities;
using appWSVentas.Interfaces;
using appWSVentas.Models;
using appWSVentas.Models.Common;
using appWSVentas.Models.RequestWS;
using appWSVentas.Models.ResponseWS;
using appWSVentas.Tools;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace appWSVentas.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly AppSettings _appSettings;

        public UsuarioService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public ResponseUsuarioModel Login(RequestUsuarioModel usuario, string ipAddress)
        {
            ResponseUsuarioModel usuarioOuput = new ResponseUsuarioModel();
            using (var db = new VentaRealDbContext())
            {
                string ePassword = Encrypt.GetSHA256(usuario.Password);
                var Usuario = db.Usuarios.Where(u => u.Email == usuario.Email && u.Password == ePassword).FirstOrDefault();
                if (Usuario == null) return null;
                usuarioOuput.Email = usuario.Email;
                usuarioOuput.JwtToken = generarToken(Usuario);
                usuarioOuput.RefreshToken = generateRefreshToken(ipAddress);
            }

            return usuarioOuput;
        }

        private string generarToken(Usuario usuario)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var llave = Encoding.ASCII.GetBytes(_appSettings.Secreto);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity
                (
                    new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, usuario.Id.ToString()),
                        new Claim(ClaimTypes.Email, usuario.Email)
                    }

                ),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(llave), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public ResponseUsuarioModel refreshToken(string token, string ipAddress)
        {
            using (var db = new VentaRealDbContext())
            {
                //var user = db.Usuarios.SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

                //// return null if no user found with token
                //if (user == null) return null;

                //var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

                //// return null if token is no longer active
                //if (!refreshToken.IsActive) return null;

                //// replace old refresh token with a new one and save
                //var newRefreshToken = generateRefreshToken(ipAddress);
                //refreshToken.Revoked = DateTime.UtcNow;
                //refreshToken.RevokedByIp = ipAddress;
                //refreshToken.ReplacedByToken = newRefreshToken.Token;
                //user.RefreshTokens.Add(newRefreshToken);
                //db.Update(user);
                //db.SaveChanges();

                //// generate new jwt
                //var jwtToken = generarToken(user);
            }
                

            return new ResponseUsuarioModel() { 
                Email = "",
                JwtToken = "",
                RefreshToken = new RefreshToken()
            };
        }

        private RefreshToken generateRefreshToken(string ipAddress)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.UtcNow.AddDays(7),
                    Created = DateTime.UtcNow,
                    CreatedByIp = ipAddress
                };
            }
        }


    }
}
