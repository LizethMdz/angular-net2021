﻿using appWSVentas.Models.RequestWS;
using appWSVentas.Models.ResponseWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appWSVentas.Interfaces
{
    public interface IUsuarioService
    {
        ResponseUsuarioModel Login(RequestUsuarioModel usuario, string ipAddress);
    }
}
