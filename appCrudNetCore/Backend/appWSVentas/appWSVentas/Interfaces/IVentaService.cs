﻿using appWSVentas.Models.RequestWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace appWSVentas.Interfaces
{
    public interface IVentaService
    {
        public void AgregarVenta(RequestVentaModel venta);

        public void AgregarVenta2(RequestVentaModel venta);
    }
}
